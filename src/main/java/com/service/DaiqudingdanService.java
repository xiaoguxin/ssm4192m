package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DaiqudingdanEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DaiqudingdanVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DaiqudingdanView;


/**
 * 代取订单
 *
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DaiqudingdanService extends IService<DaiqudingdanEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DaiqudingdanVO> selectListVO(Wrapper<DaiqudingdanEntity> wrapper);
   	
   	DaiqudingdanVO selectVO(@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);
   	
   	List<DaiqudingdanView> selectListView(Wrapper<DaiqudingdanEntity> wrapper);
   	
   	DaiqudingdanView selectView(@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DaiqudingdanEntity> wrapper);
   	

}

