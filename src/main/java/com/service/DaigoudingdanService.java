package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DaigoudingdanEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DaigoudingdanVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DaigoudingdanView;


/**
 * 代购订单
 *
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DaigoudingdanService extends IService<DaigoudingdanEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DaigoudingdanVO> selectListVO(Wrapper<DaigoudingdanEntity> wrapper);
   	
   	DaigoudingdanVO selectVO(@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);
   	
   	List<DaigoudingdanView> selectListView(Wrapper<DaigoudingdanEntity> wrapper);
   	
   	DaigoudingdanView selectView(@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DaigoudingdanEntity> wrapper);
   	

}

