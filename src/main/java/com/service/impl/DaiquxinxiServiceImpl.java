package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.DaiquxinxiDao;
import com.entity.DaiquxinxiEntity;
import com.service.DaiquxinxiService;
import com.entity.vo.DaiquxinxiVO;
import com.entity.view.DaiquxinxiView;

@Service("daiquxinxiService")
public class DaiquxinxiServiceImpl extends ServiceImpl<DaiquxinxiDao, DaiquxinxiEntity> implements DaiquxinxiService {
	

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DaiquxinxiEntity> page = this.selectPage(
                new Query<DaiquxinxiEntity>(params).getPage(),
                new EntityWrapper<DaiquxinxiEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DaiquxinxiEntity> wrapper) {
		  Page<DaiquxinxiView> page =new Query<DaiquxinxiView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DaiquxinxiVO> selectListVO(Wrapper<DaiquxinxiEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DaiquxinxiVO selectVO(Wrapper<DaiquxinxiEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DaiquxinxiView> selectListView(Wrapper<DaiquxinxiEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DaiquxinxiView selectView(Wrapper<DaiquxinxiEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
