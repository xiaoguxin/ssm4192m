package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.DaigoudingdanDao;
import com.entity.DaigoudingdanEntity;
import com.service.DaigoudingdanService;
import com.entity.vo.DaigoudingdanVO;
import com.entity.view.DaigoudingdanView;

@Service("daigoudingdanService")
public class DaigoudingdanServiceImpl extends ServiceImpl<DaigoudingdanDao, DaigoudingdanEntity> implements DaigoudingdanService {
	

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DaigoudingdanEntity> page = this.selectPage(
                new Query<DaigoudingdanEntity>(params).getPage(),
                new EntityWrapper<DaigoudingdanEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DaigoudingdanEntity> wrapper) {
		  Page<DaigoudingdanView> page =new Query<DaigoudingdanView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DaigoudingdanVO> selectListVO(Wrapper<DaigoudingdanEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DaigoudingdanVO selectVO(Wrapper<DaigoudingdanEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DaigoudingdanView> selectListView(Wrapper<DaigoudingdanEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DaigoudingdanView selectView(Wrapper<DaigoudingdanEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
