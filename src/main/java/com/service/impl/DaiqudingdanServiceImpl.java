package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.DaiqudingdanDao;
import com.entity.DaiqudingdanEntity;
import com.service.DaiqudingdanService;
import com.entity.vo.DaiqudingdanVO;
import com.entity.view.DaiqudingdanView;

@Service("daiqudingdanService")
public class DaiqudingdanServiceImpl extends ServiceImpl<DaiqudingdanDao, DaiqudingdanEntity> implements DaiqudingdanService {
	

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DaiqudingdanEntity> page = this.selectPage(
                new Query<DaiqudingdanEntity>(params).getPage(),
                new EntityWrapper<DaiqudingdanEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DaiqudingdanEntity> wrapper) {
		  Page<DaiqudingdanView> page =new Query<DaiqudingdanView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DaiqudingdanVO> selectListVO(Wrapper<DaiqudingdanEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DaiqudingdanVO selectVO(Wrapper<DaiqudingdanEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DaiqudingdanView> selectListView(Wrapper<DaiqudingdanEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DaiqudingdanView selectView(Wrapper<DaiqudingdanEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
