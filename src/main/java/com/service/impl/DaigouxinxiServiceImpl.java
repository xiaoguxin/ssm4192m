package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.DaigouxinxiDao;
import com.entity.DaigouxinxiEntity;
import com.service.DaigouxinxiService;
import com.entity.vo.DaigouxinxiVO;
import com.entity.view.DaigouxinxiView;

@Service("daigouxinxiService")
public class DaigouxinxiServiceImpl extends ServiceImpl<DaigouxinxiDao, DaigouxinxiEntity> implements DaigouxinxiService {
	

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DaigouxinxiEntity> page = this.selectPage(
                new Query<DaigouxinxiEntity>(params).getPage(),
                new EntityWrapper<DaigouxinxiEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DaigouxinxiEntity> wrapper) {
		  Page<DaigouxinxiView> page =new Query<DaigouxinxiView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DaigouxinxiVO> selectListVO(Wrapper<DaigouxinxiEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DaigouxinxiVO selectVO(Wrapper<DaigouxinxiEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DaigouxinxiView> selectListView(Wrapper<DaigouxinxiEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DaigouxinxiView selectView(Wrapper<DaigouxinxiEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
