package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.WanchengdaigouDao;
import com.entity.WanchengdaigouEntity;
import com.service.WanchengdaigouService;
import com.entity.vo.WanchengdaigouVO;
import com.entity.view.WanchengdaigouView;

@Service("wanchengdaigouService")
public class WanchengdaigouServiceImpl extends ServiceImpl<WanchengdaigouDao, WanchengdaigouEntity> implements WanchengdaigouService {
	

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<WanchengdaigouEntity> page = this.selectPage(
                new Query<WanchengdaigouEntity>(params).getPage(),
                new EntityWrapper<WanchengdaigouEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<WanchengdaigouEntity> wrapper) {
		  Page<WanchengdaigouView> page =new Query<WanchengdaigouView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<WanchengdaigouVO> selectListVO(Wrapper<WanchengdaigouEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public WanchengdaigouVO selectVO(Wrapper<WanchengdaigouEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<WanchengdaigouView> selectListView(Wrapper<WanchengdaigouEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public WanchengdaigouView selectView(Wrapper<WanchengdaigouEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
