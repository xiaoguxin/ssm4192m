package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.DiscussdaiquxinxiDao;
import com.entity.DiscussdaiquxinxiEntity;
import com.service.DiscussdaiquxinxiService;
import com.entity.vo.DiscussdaiquxinxiVO;
import com.entity.view.DiscussdaiquxinxiView;

@Service("discussdaiquxinxiService")
public class DiscussdaiquxinxiServiceImpl extends ServiceImpl<DiscussdaiquxinxiDao, DiscussdaiquxinxiEntity> implements DiscussdaiquxinxiService {
	

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DiscussdaiquxinxiEntity> page = this.selectPage(
                new Query<DiscussdaiquxinxiEntity>(params).getPage(),
                new EntityWrapper<DiscussdaiquxinxiEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<DiscussdaiquxinxiEntity> wrapper) {
		  Page<DiscussdaiquxinxiView> page =new Query<DiscussdaiquxinxiView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    
    @Override
	public List<DiscussdaiquxinxiVO> selectListVO(Wrapper<DiscussdaiquxinxiEntity> wrapper) {
 		return baseMapper.selectListVO(wrapper);
	}
	
	@Override
	public DiscussdaiquxinxiVO selectVO(Wrapper<DiscussdaiquxinxiEntity> wrapper) {
 		return baseMapper.selectVO(wrapper);
	}
	
	@Override
	public List<DiscussdaiquxinxiView> selectListView(Wrapper<DiscussdaiquxinxiEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public DiscussdaiquxinxiView selectView(Wrapper<DiscussdaiquxinxiEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
