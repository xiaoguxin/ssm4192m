package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DaigouxinxiEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DaigouxinxiVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DaigouxinxiView;


/**
 * 代购信息
 *
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DaigouxinxiService extends IService<DaigouxinxiEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DaigouxinxiVO> selectListVO(Wrapper<DaigouxinxiEntity> wrapper);
   	
   	DaigouxinxiVO selectVO(@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);
   	
   	List<DaigouxinxiView> selectListView(Wrapper<DaigouxinxiEntity> wrapper);
   	
   	DaigouxinxiView selectView(@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DaigouxinxiEntity> wrapper);
   	

}

