package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.WanchengdaigouEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.WanchengdaigouVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.WanchengdaigouView;


/**
 * 完成代购
 *
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface WanchengdaigouService extends IService<WanchengdaigouEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<WanchengdaigouVO> selectListVO(Wrapper<WanchengdaigouEntity> wrapper);
   	
   	WanchengdaigouVO selectVO(@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);
   	
   	List<WanchengdaigouView> selectListView(Wrapper<WanchengdaigouEntity> wrapper);
   	
   	WanchengdaigouView selectView(@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<WanchengdaigouEntity> wrapper);
   	

}

