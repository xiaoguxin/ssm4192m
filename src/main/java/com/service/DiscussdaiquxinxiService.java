package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DiscussdaiquxinxiEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DiscussdaiquxinxiVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DiscussdaiquxinxiView;


/**
 * 代取信息评论表
 *
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DiscussdaiquxinxiService extends IService<DiscussdaiquxinxiEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DiscussdaiquxinxiVO> selectListVO(Wrapper<DiscussdaiquxinxiEntity> wrapper);
   	
   	DiscussdaiquxinxiVO selectVO(@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);
   	
   	List<DiscussdaiquxinxiView> selectListView(Wrapper<DiscussdaiquxinxiEntity> wrapper);
   	
   	DiscussdaiquxinxiView selectView(@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DiscussdaiquxinxiEntity> wrapper);
   	

}

