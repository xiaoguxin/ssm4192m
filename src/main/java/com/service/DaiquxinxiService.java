package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.entity.DaiquxinxiEntity;
import java.util.List;
import java.util.Map;
import com.entity.vo.DaiquxinxiVO;
import org.apache.ibatis.annotations.Param;
import com.entity.view.DaiquxinxiView;


/**
 * 代取信息
 *
 * @author 
 * @email 
 * @date 2023-05-11 17:05:49
 */
public interface DaiquxinxiService extends IService<DaiquxinxiEntity> {

    PageUtils queryPage(Map<String, Object> params);
    
   	List<DaiquxinxiVO> selectListVO(Wrapper<DaiquxinxiEntity> wrapper);
   	
   	DaiquxinxiVO selectVO(@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);
   	
   	List<DaiquxinxiView> selectListView(Wrapper<DaiquxinxiEntity> wrapper);
   	
   	DaiquxinxiView selectView(@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<DaiquxinxiEntity> wrapper);
   	

}

