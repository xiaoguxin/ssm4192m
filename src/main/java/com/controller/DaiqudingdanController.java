package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.DaiqudingdanEntity;
import com.entity.view.DaiqudingdanView;

import com.service.DaiqudingdanService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MD5Util;
import com.utils.MPUtil;
import com.utils.CommonUtil;

/**
 * 代取订单
 * 后端接口
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@RestController
@RequestMapping("/daiqudingdan")
public class DaiqudingdanController {
    @Autowired
    private DaiqudingdanService daiqudingdanService;




    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,DaiqudingdanEntity daiqudingdan, 
		HttpServletRequest request){

        EntityWrapper<DaiqudingdanEntity> ew = new EntityWrapper<DaiqudingdanEntity>();


		String tableName = request.getSession().getAttribute("tableName").toString();
        ew.andNew();
		if(tableName.equals("xuesheng")) {
            ew.eq("xuehao", (String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("xuesheng")) {
            ew.or();
            ew.eq("daiquzhanghao", (String)request.getSession().getAttribute("username"));
		}
		PageUtils page = daiqudingdanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, daiqudingdan), params), params));
        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,DaiqudingdanEntity daiqudingdan, 
		HttpServletRequest request){
        EntityWrapper<DaiqudingdanEntity> ew = new EntityWrapper<DaiqudingdanEntity>();

		PageUtils page = daiqudingdanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, daiqudingdan), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( DaiqudingdanEntity daiqudingdan){
       	EntityWrapper<DaiqudingdanEntity> ew = new EntityWrapper<DaiqudingdanEntity>();
      	ew.allEq(MPUtil.allEQMapPre( daiqudingdan, "daiqudingdan")); 
        return R.ok().put("data", daiqudingdanService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(DaiqudingdanEntity daiqudingdan){
        EntityWrapper< DaiqudingdanEntity> ew = new EntityWrapper< DaiqudingdanEntity>();
 		ew.allEq(MPUtil.allEQMapPre( daiqudingdan, "daiqudingdan")); 
		DaiqudingdanView daiqudingdanView =  daiqudingdanService.selectView(ew);
		return R.ok("查询代取订单成功").put("data", daiqudingdanView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        DaiqudingdanEntity daiqudingdan = daiqudingdanService.selectById(id);
        return R.ok().put("data", daiqudingdan);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        DaiqudingdanEntity daiqudingdan = daiqudingdanService.selectById(id);
        return R.ok().put("data", daiqudingdan);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DaiqudingdanEntity daiqudingdan, HttpServletRequest request){
    	daiqudingdan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(daiqudingdan);

        daiqudingdanService.insert(daiqudingdan);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody DaiqudingdanEntity daiqudingdan, HttpServletRequest request){
    	daiqudingdan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(daiqudingdan);

        daiqudingdanService.insert(daiqudingdan);
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody DaiqudingdanEntity daiqudingdan, HttpServletRequest request){
        //ValidatorUtils.validateEntity(daiqudingdan);
        daiqudingdanService.updateById(daiqudingdan);//全部更新
        return R.ok();
    }

    /**
     * 审核
     */
    @RequestMapping("/shBatch")
    @Transactional
    public R update(@RequestBody Long[] ids, @RequestParam String sfsh, @RequestParam String shhf){
        List<DaiqudingdanEntity> list = new ArrayList<DaiqudingdanEntity>();
        for(Long id : ids) {
            DaiqudingdanEntity daiqudingdan = daiqudingdanService.selectById(id);
            daiqudingdan.setSfsh(sfsh);
            daiqudingdan.setShhf(shhf);
            list.add(daiqudingdan);
        }
        daiqudingdanService.updateBatchById(list);
        return R.ok();
    }
    
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        daiqudingdanService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	









}
