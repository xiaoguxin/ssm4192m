package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.DaigouxinxiEntity;
import com.entity.view.DaigouxinxiView;

import com.service.DaigouxinxiService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MD5Util;
import com.utils.MPUtil;
import com.utils.CommonUtil;

/**
 * 代购信息
 * 后端接口
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@RestController
@RequestMapping("/daigouxinxi")
public class DaigouxinxiController {
    @Autowired
    private DaigouxinxiService daigouxinxiService;




    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,DaigouxinxiEntity daigouxinxi, 
		HttpServletRequest request){

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("xuesheng")) {
			daigouxinxi.setXuehao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<DaigouxinxiEntity> ew = new EntityWrapper<DaigouxinxiEntity>();


		PageUtils page = daigouxinxiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, daigouxinxi), params), params));
        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,DaigouxinxiEntity daigouxinxi, 
		HttpServletRequest request){
        EntityWrapper<DaigouxinxiEntity> ew = new EntityWrapper<DaigouxinxiEntity>();

		PageUtils page = daigouxinxiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, daigouxinxi), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( DaigouxinxiEntity daigouxinxi){
       	EntityWrapper<DaigouxinxiEntity> ew = new EntityWrapper<DaigouxinxiEntity>();
      	ew.allEq(MPUtil.allEQMapPre( daigouxinxi, "daigouxinxi")); 
        return R.ok().put("data", daigouxinxiService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(DaigouxinxiEntity daigouxinxi){
        EntityWrapper< DaigouxinxiEntity> ew = new EntityWrapper< DaigouxinxiEntity>();
 		ew.allEq(MPUtil.allEQMapPre( daigouxinxi, "daigouxinxi")); 
		DaigouxinxiView daigouxinxiView =  daigouxinxiService.selectView(ew);
		return R.ok("查询代购信息成功").put("data", daigouxinxiView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        DaigouxinxiEntity daigouxinxi = daigouxinxiService.selectById(id);
        return R.ok().put("data", daigouxinxi);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        DaigouxinxiEntity daigouxinxi = daigouxinxiService.selectById(id);
        return R.ok().put("data", daigouxinxi);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DaigouxinxiEntity daigouxinxi, HttpServletRequest request){
    	daigouxinxi.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(daigouxinxi);

        daigouxinxiService.insert(daigouxinxi);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody DaigouxinxiEntity daigouxinxi, HttpServletRequest request){
    	daigouxinxi.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(daigouxinxi);

        daigouxinxiService.insert(daigouxinxi);
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody DaigouxinxiEntity daigouxinxi, HttpServletRequest request){
        //ValidatorUtils.validateEntity(daigouxinxi);
        daigouxinxiService.updateById(daigouxinxi);//全部更新
        return R.ok();
    }

    /**
     * 审核
     */
    @RequestMapping("/shBatch")
    @Transactional
    public R update(@RequestBody Long[] ids, @RequestParam String sfsh, @RequestParam String shhf){
        List<DaigouxinxiEntity> list = new ArrayList<DaigouxinxiEntity>();
        for(Long id : ids) {
            DaigouxinxiEntity daigouxinxi = daigouxinxiService.selectById(id);
            daigouxinxi.setSfsh(sfsh);
            daigouxinxi.setShhf(shhf);
            list.add(daigouxinxi);
        }
        daigouxinxiService.updateBatchById(list);
        return R.ok();
    }
    
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        daigouxinxiService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	









}
