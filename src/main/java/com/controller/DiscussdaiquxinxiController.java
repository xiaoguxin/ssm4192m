package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.DiscussdaiquxinxiEntity;
import com.entity.view.DiscussdaiquxinxiView;

import com.service.DiscussdaiquxinxiService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MD5Util;
import com.utils.MPUtil;
import com.utils.CommonUtil;

/**
 * 代取信息评论表
 * 后端接口
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@RestController
@RequestMapping("/discussdaiquxinxi")
public class DiscussdaiquxinxiController {
    @Autowired
    private DiscussdaiquxinxiService discussdaiquxinxiService;




    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,DiscussdaiquxinxiEntity discussdaiquxinxi, 
		HttpServletRequest request){

        EntityWrapper<DiscussdaiquxinxiEntity> ew = new EntityWrapper<DiscussdaiquxinxiEntity>();


		PageUtils page = discussdaiquxinxiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, discussdaiquxinxi), params), params));
        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,DiscussdaiquxinxiEntity discussdaiquxinxi, 
		HttpServletRequest request){
        EntityWrapper<DiscussdaiquxinxiEntity> ew = new EntityWrapper<DiscussdaiquxinxiEntity>();

		PageUtils page = discussdaiquxinxiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, discussdaiquxinxi), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( DiscussdaiquxinxiEntity discussdaiquxinxi){
       	EntityWrapper<DiscussdaiquxinxiEntity> ew = new EntityWrapper<DiscussdaiquxinxiEntity>();
      	ew.allEq(MPUtil.allEQMapPre( discussdaiquxinxi, "discussdaiquxinxi")); 
        return R.ok().put("data", discussdaiquxinxiService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(DiscussdaiquxinxiEntity discussdaiquxinxi){
        EntityWrapper< DiscussdaiquxinxiEntity> ew = new EntityWrapper< DiscussdaiquxinxiEntity>();
 		ew.allEq(MPUtil.allEQMapPre( discussdaiquxinxi, "discussdaiquxinxi")); 
		DiscussdaiquxinxiView discussdaiquxinxiView =  discussdaiquxinxiService.selectView(ew);
		return R.ok("查询代取信息评论表成功").put("data", discussdaiquxinxiView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        DiscussdaiquxinxiEntity discussdaiquxinxi = discussdaiquxinxiService.selectById(id);
        return R.ok().put("data", discussdaiquxinxi);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        DiscussdaiquxinxiEntity discussdaiquxinxi = discussdaiquxinxiService.selectById(id);
        return R.ok().put("data", discussdaiquxinxi);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DiscussdaiquxinxiEntity discussdaiquxinxi, HttpServletRequest request){
    	discussdaiquxinxi.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(discussdaiquxinxi);

        discussdaiquxinxiService.insert(discussdaiquxinxi);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody DiscussdaiquxinxiEntity discussdaiquxinxi, HttpServletRequest request){
    	discussdaiquxinxi.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(discussdaiquxinxi);

        discussdaiquxinxiService.insert(discussdaiquxinxi);
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody DiscussdaiquxinxiEntity discussdaiquxinxi, HttpServletRequest request){
        //ValidatorUtils.validateEntity(discussdaiquxinxi);
        discussdaiquxinxiService.updateById(discussdaiquxinxi);//全部更新
        return R.ok();
    }

    
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        discussdaiquxinxiService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	









}
