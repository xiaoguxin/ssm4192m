package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.WanchengdaigouEntity;
import com.entity.view.WanchengdaigouView;

import com.service.WanchengdaigouService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MD5Util;
import com.utils.MPUtil;
import com.utils.CommonUtil;

/**
 * 完成代购
 * 后端接口
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@RestController
@RequestMapping("/wanchengdaigou")
public class WanchengdaigouController {
    @Autowired
    private WanchengdaigouService wanchengdaigouService;




    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,WanchengdaigouEntity wanchengdaigou, 
		HttpServletRequest request){

        EntityWrapper<WanchengdaigouEntity> ew = new EntityWrapper<WanchengdaigouEntity>();


		String tableName = request.getSession().getAttribute("tableName").toString();
        ew.andNew();
		if(tableName.equals("xuesheng")) {
            ew.eq("xuehao", (String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("xuesheng")) {
            ew.or();
            ew.eq("daigouzhanghao", (String)request.getSession().getAttribute("username"));
		}
		PageUtils page = wanchengdaigouService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, wanchengdaigou), params), params));
        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,WanchengdaigouEntity wanchengdaigou, 
		HttpServletRequest request){
        EntityWrapper<WanchengdaigouEntity> ew = new EntityWrapper<WanchengdaigouEntity>();

		PageUtils page = wanchengdaigouService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, wanchengdaigou), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( WanchengdaigouEntity wanchengdaigou){
       	EntityWrapper<WanchengdaigouEntity> ew = new EntityWrapper<WanchengdaigouEntity>();
      	ew.allEq(MPUtil.allEQMapPre( wanchengdaigou, "wanchengdaigou")); 
        return R.ok().put("data", wanchengdaigouService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(WanchengdaigouEntity wanchengdaigou){
        EntityWrapper< WanchengdaigouEntity> ew = new EntityWrapper< WanchengdaigouEntity>();
 		ew.allEq(MPUtil.allEQMapPre( wanchengdaigou, "wanchengdaigou")); 
		WanchengdaigouView wanchengdaigouView =  wanchengdaigouService.selectView(ew);
		return R.ok("查询完成代购成功").put("data", wanchengdaigouView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        WanchengdaigouEntity wanchengdaigou = wanchengdaigouService.selectById(id);
        return R.ok().put("data", wanchengdaigou);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        WanchengdaigouEntity wanchengdaigou = wanchengdaigouService.selectById(id);
        return R.ok().put("data", wanchengdaigou);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WanchengdaigouEntity wanchengdaigou, HttpServletRequest request){
    	wanchengdaigou.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(wanchengdaigou);

        wanchengdaigouService.insert(wanchengdaigou);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody WanchengdaigouEntity wanchengdaigou, HttpServletRequest request){
    	wanchengdaigou.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(wanchengdaigou);

        wanchengdaigouService.insert(wanchengdaigou);
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody WanchengdaigouEntity wanchengdaigou, HttpServletRequest request){
        //ValidatorUtils.validateEntity(wanchengdaigou);
        wanchengdaigouService.updateById(wanchengdaigou);//全部更新
        return R.ok();
    }

    /**
     * 审核
     */
    @RequestMapping("/shBatch")
    @Transactional
    public R update(@RequestBody Long[] ids, @RequestParam String sfsh, @RequestParam String shhf){
        List<WanchengdaigouEntity> list = new ArrayList<WanchengdaigouEntity>();
        for(Long id : ids) {
            WanchengdaigouEntity wanchengdaigou = wanchengdaigouService.selectById(id);
            wanchengdaigou.setSfsh(sfsh);
            wanchengdaigou.setShhf(shhf);
            list.add(wanchengdaigou);
        }
        wanchengdaigouService.updateBatchById(list);
        return R.ok();
    }
    
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        wanchengdaigouService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	









}
