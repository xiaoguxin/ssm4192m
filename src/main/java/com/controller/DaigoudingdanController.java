package com.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import com.utils.ValidatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.entity.DaigoudingdanEntity;
import com.entity.view.DaigoudingdanView;

import com.service.DaigoudingdanService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MD5Util;
import com.utils.MPUtil;
import com.utils.CommonUtil;

/**
 * 代购订单
 * 后端接口
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@RestController
@RequestMapping("/daigoudingdan")
public class DaigoudingdanController {
    @Autowired
    private DaigoudingdanService daigoudingdanService;




    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,DaigoudingdanEntity daigoudingdan, 
		HttpServletRequest request){

        EntityWrapper<DaigoudingdanEntity> ew = new EntityWrapper<DaigoudingdanEntity>();


		String tableName = request.getSession().getAttribute("tableName").toString();
        ew.andNew();
		if(tableName.equals("xuesheng")) {
            ew.eq("xuehao", (String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("xuesheng")) {
            ew.or();
            ew.eq("daigouzhanghao", (String)request.getSession().getAttribute("username"));
		}
		PageUtils page = daigoudingdanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, daigoudingdan), params), params));
        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,DaigoudingdanEntity daigoudingdan, 
		HttpServletRequest request){
        EntityWrapper<DaigoudingdanEntity> ew = new EntityWrapper<DaigoudingdanEntity>();

		PageUtils page = daigoudingdanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, daigoudingdan), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( DaigoudingdanEntity daigoudingdan){
       	EntityWrapper<DaigoudingdanEntity> ew = new EntityWrapper<DaigoudingdanEntity>();
      	ew.allEq(MPUtil.allEQMapPre( daigoudingdan, "daigoudingdan")); 
        return R.ok().put("data", daigoudingdanService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(DaigoudingdanEntity daigoudingdan){
        EntityWrapper< DaigoudingdanEntity> ew = new EntityWrapper< DaigoudingdanEntity>();
 		ew.allEq(MPUtil.allEQMapPre( daigoudingdan, "daigoudingdan")); 
		DaigoudingdanView daigoudingdanView =  daigoudingdanService.selectView(ew);
		return R.ok("查询代购订单成功").put("data", daigoudingdanView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        DaigoudingdanEntity daigoudingdan = daigoudingdanService.selectById(id);
        return R.ok().put("data", daigoudingdan);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        DaigoudingdanEntity daigoudingdan = daigoudingdanService.selectById(id);
        return R.ok().put("data", daigoudingdan);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody DaigoudingdanEntity daigoudingdan, HttpServletRequest request){
    	daigoudingdan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(daigoudingdan);

        daigoudingdanService.insert(daigoudingdan);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody DaigoudingdanEntity daigoudingdan, HttpServletRequest request){
    	daigoudingdan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(daigoudingdan);

        daigoudingdanService.insert(daigoudingdan);
        return R.ok();
    }


    /**
     * 修改
     */
    @RequestMapping("/update")
    @Transactional
    public R update(@RequestBody DaigoudingdanEntity daigoudingdan, HttpServletRequest request){
        //ValidatorUtils.validateEntity(daigoudingdan);
        daigoudingdanService.updateById(daigoudingdan);//全部更新
        return R.ok();
    }

    /**
     * 审核
     */
    @RequestMapping("/shBatch")
    @Transactional
    public R update(@RequestBody Long[] ids, @RequestParam String sfsh, @RequestParam String shhf){
        List<DaigoudingdanEntity> list = new ArrayList<DaigoudingdanEntity>();
        for(Long id : ids) {
            DaigoudingdanEntity daigoudingdan = daigoudingdanService.selectById(id);
            daigoudingdan.setSfsh(sfsh);
            daigoudingdan.setShhf(shhf);
            list.add(daigoudingdan);
        }
        daigoudingdanService.updateBatchById(list);
        return R.ok();
    }
    
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        daigoudingdanService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
	









}
