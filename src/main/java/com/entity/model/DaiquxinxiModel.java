package com.entity.model;

import com.entity.DaiquxinxiEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;
 

/**
 * 代取信息
 * 接收传参的实体类  
 *（实际开发中配合移动端接口开发手动去掉些没用的字段， 后端一般用entity就够用了） 
 * 取自ModelAndView 的model名称
 * @author 
 * @email 
 * @date 2023-05-11 17:05:49
 */
public class DaiquxinxiModel  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 商品信息
	 */
	
	private String shangpinxinxi;
		
	/**
	 * 照片
	 */
	
	private String zhaopian;
		
	/**
	 * 商品数量
	 */
	
	private Integer shangpinshuliang;
		
	/**
	 * 商品重量
	 */
	
	private Float shangpinzhongliang;
		
	/**
	 * 超重费用
	 */
	
	private Float chaozhongfeiyong;
		
	/**
	 * 地址
	 */
	
	private String dizhi;
		
	/**
	 * 发布时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date fabushijian;
		
	/**
	 * 代取备注
	 */
	
	private String daiqubeizhu;
		
	/**
	 * 学号
	 */
	
	private String xuehao;
		
	/**
	 * 姓名
	 */
	
	private String xingming;
		
	/**
	 * 联系电话
	 */
	
	private String lianxidianhua;
		
	/**
	 * 代取状态
	 */
	
	private String daiquzhuangtai;
		
	/**
	 * 是否审核
	 */
	
	private String sfsh;
		
	/**
	 * 审核回复
	 */
	
	private String shhf;
				
	
	/**
	 * 设置：商品信息
	 */
	 
	public void setShangpinxinxi(String shangpinxinxi) {
		this.shangpinxinxi = shangpinxinxi;
	}
	
	/**
	 * 获取：商品信息
	 */
	public String getShangpinxinxi() {
		return shangpinxinxi;
	}
				
	
	/**
	 * 设置：照片
	 */
	 
	public void setZhaopian(String zhaopian) {
		this.zhaopian = zhaopian;
	}
	
	/**
	 * 获取：照片
	 */
	public String getZhaopian() {
		return zhaopian;
	}
				
	
	/**
	 * 设置：商品数量
	 */
	 
	public void setShangpinshuliang(Integer shangpinshuliang) {
		this.shangpinshuliang = shangpinshuliang;
	}
	
	/**
	 * 获取：商品数量
	 */
	public Integer getShangpinshuliang() {
		return shangpinshuliang;
	}
				
	
	/**
	 * 设置：商品重量
	 */
	 
	public void setShangpinzhongliang(Float shangpinzhongliang) {
		this.shangpinzhongliang = shangpinzhongliang;
	}
	
	/**
	 * 获取：商品重量
	 */
	public Float getShangpinzhongliang() {
		return shangpinzhongliang;
	}
				
	
	/**
	 * 设置：超重费用
	 */
	 
	public void setChaozhongfeiyong(Float chaozhongfeiyong) {
		this.chaozhongfeiyong = chaozhongfeiyong;
	}
	
	/**
	 * 获取：超重费用
	 */
	public Float getChaozhongfeiyong() {
		return chaozhongfeiyong;
	}
				
	
	/**
	 * 设置：地址
	 */
	 
	public void setDizhi(String dizhi) {
		this.dizhi = dizhi;
	}
	
	/**
	 * 获取：地址
	 */
	public String getDizhi() {
		return dizhi;
	}
				
	
	/**
	 * 设置：发布时间
	 */
	 
	public void setFabushijian(Date fabushijian) {
		this.fabushijian = fabushijian;
	}
	
	/**
	 * 获取：发布时间
	 */
	public Date getFabushijian() {
		return fabushijian;
	}
				
	
	/**
	 * 设置：代取备注
	 */
	 
	public void setDaiqubeizhu(String daiqubeizhu) {
		this.daiqubeizhu = daiqubeizhu;
	}
	
	/**
	 * 获取：代取备注
	 */
	public String getDaiqubeizhu() {
		return daiqubeizhu;
	}
				
	
	/**
	 * 设置：学号
	 */
	 
	public void setXuehao(String xuehao) {
		this.xuehao = xuehao;
	}
	
	/**
	 * 获取：学号
	 */
	public String getXuehao() {
		return xuehao;
	}
				
	
	/**
	 * 设置：姓名
	 */
	 
	public void setXingming(String xingming) {
		this.xingming = xingming;
	}
	
	/**
	 * 获取：姓名
	 */
	public String getXingming() {
		return xingming;
	}
				
	
	/**
	 * 设置：联系电话
	 */
	 
	public void setLianxidianhua(String lianxidianhua) {
		this.lianxidianhua = lianxidianhua;
	}
	
	/**
	 * 获取：联系电话
	 */
	public String getLianxidianhua() {
		return lianxidianhua;
	}
				
	
	/**
	 * 设置：代取状态
	 */
	 
	public void setDaiquzhuangtai(String daiquzhuangtai) {
		this.daiquzhuangtai = daiquzhuangtai;
	}
	
	/**
	 * 获取：代取状态
	 */
	public String getDaiquzhuangtai() {
		return daiquzhuangtai;
	}
				
	
	/**
	 * 设置：是否审核
	 */
	 
	public void setSfsh(String sfsh) {
		this.sfsh = sfsh;
	}
	
	/**
	 * 获取：是否审核
	 */
	public String getSfsh() {
		return sfsh;
	}
				
	
	/**
	 * 设置：审核回复
	 */
	 
	public void setShhf(String shhf) {
		this.shhf = shhf;
	}
	
	/**
	 * 获取：审核回复
	 */
	public String getShhf() {
		return shhf;
	}
			
}
