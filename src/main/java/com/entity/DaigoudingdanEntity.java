package com.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.enums.IdType;


/**
 * 代购订单
 * 数据库通用操作实体类（普通增删改查）
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@TableName("daigoudingdan")
public class DaigoudingdanEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public DaigoudingdanEntity() {
		
	}
	
	public DaigoudingdanEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 主键id
	 */
	@TableId
	private Long id;
	/**
	 * 订单编号
	 */
					
	private String dingdanbianhao;
	
	/**
	 * 商品名称
	 */
					
	private String shangpinmingcheng;
	
	/**
	 * 商品价格
	 */
					
	private Float shangpinjiage;
	
	/**
	 * 商品重量
	 */
					
	private Float shangpinzhongliang;
	
	/**
	 * 超重费用
	 */
					
	private Float chaozhongfeiyong;
	
	/**
	 * 定金比例/%
	 */
					
	private Float dingjinbili;
	
	/**
	 * 定金
	 */
					
	private Float dingjin;
	
	/**
	 * 收货地址
	 */
					
	private String shouhuodizhi;
	
	/**
	 * 购物地址
	 */
					
	private String gouwudizhi;
	
	/**
	 * 购物说明
	 */
					
	private String gouwushuoming;
	
	/**
	 * 最晚完成时间
	 */
					
	private String zuiwanwanchengshijian;
	
	/**
	 * 订单时间
	 */
				
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	private Date dingdanshijian;
	
	/**
	 * 发布者
	 */
					
	private String xuehao;
	
	/**
	 * 代购者
	 */
					
	private String daigouzhanghao;
	
	/**
	 * 是否审核
	 */
					
	private String sfsh;
	
	/**
	 * 审核回复
	 */
					
	private String shhf;
	
	/**
	 * 是否支付
	 */
					
	private String ispay;
	
	
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat
	private Date addtime;

	public Date getAddtime() {
		return addtime;
	}
	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 设置：订单编号
	 */
	public void setDingdanbianhao(String dingdanbianhao) {
		this.dingdanbianhao = dingdanbianhao;
	}
	/**
	 * 获取：订单编号
	 */
	public String getDingdanbianhao() {
		return dingdanbianhao;
	}
	/**
	 * 设置：商品名称
	 */
	public void setShangpinmingcheng(String shangpinmingcheng) {
		this.shangpinmingcheng = shangpinmingcheng;
	}
	/**
	 * 获取：商品名称
	 */
	public String getShangpinmingcheng() {
		return shangpinmingcheng;
	}
	/**
	 * 设置：商品价格
	 */
	public void setShangpinjiage(Float shangpinjiage) {
		this.shangpinjiage = shangpinjiage;
	}
	/**
	 * 获取：商品价格
	 */
	public Float getShangpinjiage() {
		return shangpinjiage;
	}
	/**
	 * 设置：商品重量
	 */
	public void setShangpinzhongliang(Float shangpinzhongliang) {
		this.shangpinzhongliang = shangpinzhongliang;
	}
	/**
	 * 获取：商品重量
	 */
	public Float getShangpinzhongliang() {
		return shangpinzhongliang;
	}
	/**
	 * 设置：超重费用
	 */
	public void setChaozhongfeiyong(Float chaozhongfeiyong) {
		this.chaozhongfeiyong = chaozhongfeiyong;
	}
	/**
	 * 获取：超重费用
	 */
	public Float getChaozhongfeiyong() {
		return chaozhongfeiyong;
	}
	/**
	 * 设置：定金比例/%
	 */
	public void setDingjinbili(Float dingjinbili) {
		this.dingjinbili = dingjinbili;
	}
	/**
	 * 获取：定金比例/%
	 */
	public Float getDingjinbili() {
		return dingjinbili;
	}
	/**
	 * 设置：定金
	 */
	public void setDingjin(Float dingjin) {
		this.dingjin = dingjin;
	}
	/**
	 * 获取：定金
	 */
	public Float getDingjin() {
		return dingjin;
	}
	/**
	 * 设置：收货地址
	 */
	public void setShouhuodizhi(String shouhuodizhi) {
		this.shouhuodizhi = shouhuodizhi;
	}
	/**
	 * 获取：收货地址
	 */
	public String getShouhuodizhi() {
		return shouhuodizhi;
	}
	/**
	 * 设置：购物地址
	 */
	public void setGouwudizhi(String gouwudizhi) {
		this.gouwudizhi = gouwudizhi;
	}
	/**
	 * 获取：购物地址
	 */
	public String getGouwudizhi() {
		return gouwudizhi;
	}
	/**
	 * 设置：购物说明
	 */
	public void setGouwushuoming(String gouwushuoming) {
		this.gouwushuoming = gouwushuoming;
	}
	/**
	 * 获取：购物说明
	 */
	public String getGouwushuoming() {
		return gouwushuoming;
	}
	/**
	 * 设置：最晚完成时间
	 */
	public void setZuiwanwanchengshijian(String zuiwanwanchengshijian) {
		this.zuiwanwanchengshijian = zuiwanwanchengshijian;
	}
	/**
	 * 获取：最晚完成时间
	 */
	public String getZuiwanwanchengshijian() {
		return zuiwanwanchengshijian;
	}
	/**
	 * 设置：订单时间
	 */
	public void setDingdanshijian(Date dingdanshijian) {
		this.dingdanshijian = dingdanshijian;
	}
	/**
	 * 获取：订单时间
	 */
	public Date getDingdanshijian() {
		return dingdanshijian;
	}
	/**
	 * 设置：发布者
	 */
	public void setXuehao(String xuehao) {
		this.xuehao = xuehao;
	}
	/**
	 * 获取：发布者
	 */
	public String getXuehao() {
		return xuehao;
	}
	/**
	 * 设置：代购者
	 */
	public void setDaigouzhanghao(String daigouzhanghao) {
		this.daigouzhanghao = daigouzhanghao;
	}
	/**
	 * 获取：代购者
	 */
	public String getDaigouzhanghao() {
		return daigouzhanghao;
	}
	/**
	 * 设置：是否审核
	 */
	public void setSfsh(String sfsh) {
		this.sfsh = sfsh;
	}
	/**
	 * 获取：是否审核
	 */
	public String getSfsh() {
		return sfsh;
	}
	/**
	 * 设置：审核回复
	 */
	public void setShhf(String shhf) {
		this.shhf = shhf;
	}
	/**
	 * 获取：审核回复
	 */
	public String getShhf() {
		return shhf;
	}
	/**
	 * 设置：是否支付
	 */
	public void setIspay(String ispay) {
		this.ispay = ispay;
	}
	/**
	 * 获取：是否支付
	 */
	public String getIspay() {
		return ispay;
	}

}
