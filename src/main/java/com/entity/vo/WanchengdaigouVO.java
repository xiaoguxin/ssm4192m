package com.entity.vo;

import com.entity.WanchengdaigouEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
 

/**
 * 完成代购
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public class WanchengdaigouVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 商品名称
	 */
	
	private String shangpinmingcheng;
		
	/**
	 * 商品价格
	 */
	
	private Float shangpinjiage;
		
	/**
	 * 商品重量
	 */
	
	private Float shangpinzhongliang;
		
	/**
	 * 超重费用
	 */
	
	private Float chaozhongfeiyong;
		
	/**
	 * 尾款比例/%
	 */
	
	private Float weikuanbili;
		
	/**
	 * 尾款
	 */
	
	private Float weikuan;
		
	/**
	 * 收货地址
	 */
	
	private String shouhuodizhi;
		
	/**
	 * 购物地址
	 */
	
	private String gouwudizhi;
		
	/**
	 * 购物说明
	 */
	
	private String gouwushuoming;
		
	/**
	 * 最晚完成时间
	 */
	
	private String zuiwanwanchengshijian;
		
	/**
	 * 登记时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date dengjishijian;
		
	/**
	 * 发布者
	 */
	
	private String xuehao;
		
	/**
	 * 代购者
	 */
	
	private String daigouzhanghao;
		
	/**
	 * 是否审核
	 */
	
	private String sfsh;
		
	/**
	 * 审核回复
	 */
	
	private String shhf;
		
	/**
	 * 是否支付
	 */
	
	private String ispay;
				
	
	/**
	 * 设置：商品名称
	 */
	 
	public void setShangpinmingcheng(String shangpinmingcheng) {
		this.shangpinmingcheng = shangpinmingcheng;
	}
	
	/**
	 * 获取：商品名称
	 */
	public String getShangpinmingcheng() {
		return shangpinmingcheng;
	}
				
	
	/**
	 * 设置：商品价格
	 */
	 
	public void setShangpinjiage(Float shangpinjiage) {
		this.shangpinjiage = shangpinjiage;
	}
	
	/**
	 * 获取：商品价格
	 */
	public Float getShangpinjiage() {
		return shangpinjiage;
	}
				
	
	/**
	 * 设置：商品重量
	 */
	 
	public void setShangpinzhongliang(Float shangpinzhongliang) {
		this.shangpinzhongliang = shangpinzhongliang;
	}
	
	/**
	 * 获取：商品重量
	 */
	public Float getShangpinzhongliang() {
		return shangpinzhongliang;
	}
				
	
	/**
	 * 设置：超重费用
	 */
	 
	public void setChaozhongfeiyong(Float chaozhongfeiyong) {
		this.chaozhongfeiyong = chaozhongfeiyong;
	}
	
	/**
	 * 获取：超重费用
	 */
	public Float getChaozhongfeiyong() {
		return chaozhongfeiyong;
	}
				
	
	/**
	 * 设置：尾款比例/%
	 */
	 
	public void setWeikuanbili(Float weikuanbili) {
		this.weikuanbili = weikuanbili;
	}
	
	/**
	 * 获取：尾款比例/%
	 */
	public Float getWeikuanbili() {
		return weikuanbili;
	}
				
	
	/**
	 * 设置：尾款
	 */
	 
	public void setWeikuan(Float weikuan) {
		this.weikuan = weikuan;
	}
	
	/**
	 * 获取：尾款
	 */
	public Float getWeikuan() {
		return weikuan;
	}
				
	
	/**
	 * 设置：收货地址
	 */
	 
	public void setShouhuodizhi(String shouhuodizhi) {
		this.shouhuodizhi = shouhuodizhi;
	}
	
	/**
	 * 获取：收货地址
	 */
	public String getShouhuodizhi() {
		return shouhuodizhi;
	}
				
	
	/**
	 * 设置：购物地址
	 */
	 
	public void setGouwudizhi(String gouwudizhi) {
		this.gouwudizhi = gouwudizhi;
	}
	
	/**
	 * 获取：购物地址
	 */
	public String getGouwudizhi() {
		return gouwudizhi;
	}
				
	
	/**
	 * 设置：购物说明
	 */
	 
	public void setGouwushuoming(String gouwushuoming) {
		this.gouwushuoming = gouwushuoming;
	}
	
	/**
	 * 获取：购物说明
	 */
	public String getGouwushuoming() {
		return gouwushuoming;
	}
				
	
	/**
	 * 设置：最晚完成时间
	 */
	 
	public void setZuiwanwanchengshijian(String zuiwanwanchengshijian) {
		this.zuiwanwanchengshijian = zuiwanwanchengshijian;
	}
	
	/**
	 * 获取：最晚完成时间
	 */
	public String getZuiwanwanchengshijian() {
		return zuiwanwanchengshijian;
	}
				
	
	/**
	 * 设置：登记时间
	 */
	 
	public void setDengjishijian(Date dengjishijian) {
		this.dengjishijian = dengjishijian;
	}
	
	/**
	 * 获取：登记时间
	 */
	public Date getDengjishijian() {
		return dengjishijian;
	}
				
	
	/**
	 * 设置：发布者
	 */
	 
	public void setXuehao(String xuehao) {
		this.xuehao = xuehao;
	}
	
	/**
	 * 获取：发布者
	 */
	public String getXuehao() {
		return xuehao;
	}
				
	
	/**
	 * 设置：代购者
	 */
	 
	public void setDaigouzhanghao(String daigouzhanghao) {
		this.daigouzhanghao = daigouzhanghao;
	}
	
	/**
	 * 获取：代购者
	 */
	public String getDaigouzhanghao() {
		return daigouzhanghao;
	}
				
	
	/**
	 * 设置：是否审核
	 */
	 
	public void setSfsh(String sfsh) {
		this.sfsh = sfsh;
	}
	
	/**
	 * 获取：是否审核
	 */
	public String getSfsh() {
		return sfsh;
	}
				
	
	/**
	 * 设置：审核回复
	 */
	 
	public void setShhf(String shhf) {
		this.shhf = shhf;
	}
	
	/**
	 * 获取：审核回复
	 */
	public String getShhf() {
		return shhf;
	}
				
	
	/**
	 * 设置：是否支付
	 */
	 
	public void setIspay(String ispay) {
		this.ispay = ispay;
	}
	
	/**
	 * 获取：是否支付
	 */
	public String getIspay() {
		return ispay;
	}
			
}
