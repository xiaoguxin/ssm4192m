package com.entity.vo;

import com.entity.DaiqudingdanEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
 

/**
 * 代取订单
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public class DaiqudingdanVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 商品信息
	 */
	
	private String shangpinxinxi;
		
	/**
	 * 商品数量
	 */
	
	private Integer shangpinshuliang;
		
	/**
	 * 商品重量
	 */
	
	private Float shangpinzhongliang;
		
	/**
	 * 超重费用
	 */
	
	private Float chaozhongfeiyong;
		
	/**
	 * 地址
	 */
	
	private String dizhi;
		
	/**
	 * 订单时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date dingdanshijian;
		
	/**
	 * 订单备注
	 */
	
	private String dingdanbeizhu;
		
	/**
	 * 发布者
	 */
	
	private String xuehao;
		
	/**
	 * 代取者
	 */
	
	private String daiquzhanghao;
		
	/**
	 * 是否审核
	 */
	
	private String sfsh;
		
	/**
	 * 审核回复
	 */
	
	private String shhf;
		
	/**
	 * 是否支付
	 */
	
	private String ispay;
				
	
	/**
	 * 设置：商品信息
	 */
	 
	public void setShangpinxinxi(String shangpinxinxi) {
		this.shangpinxinxi = shangpinxinxi;
	}
	
	/**
	 * 获取：商品信息
	 */
	public String getShangpinxinxi() {
		return shangpinxinxi;
	}
				
	
	/**
	 * 设置：商品数量
	 */
	 
	public void setShangpinshuliang(Integer shangpinshuliang) {
		this.shangpinshuliang = shangpinshuliang;
	}
	
	/**
	 * 获取：商品数量
	 */
	public Integer getShangpinshuliang() {
		return shangpinshuliang;
	}
				
	
	/**
	 * 设置：商品重量
	 */
	 
	public void setShangpinzhongliang(Float shangpinzhongliang) {
		this.shangpinzhongliang = shangpinzhongliang;
	}
	
	/**
	 * 获取：商品重量
	 */
	public Float getShangpinzhongliang() {
		return shangpinzhongliang;
	}
				
	
	/**
	 * 设置：超重费用
	 */
	 
	public void setChaozhongfeiyong(Float chaozhongfeiyong) {
		this.chaozhongfeiyong = chaozhongfeiyong;
	}
	
	/**
	 * 获取：超重费用
	 */
	public Float getChaozhongfeiyong() {
		return chaozhongfeiyong;
	}
				
	
	/**
	 * 设置：地址
	 */
	 
	public void setDizhi(String dizhi) {
		this.dizhi = dizhi;
	}
	
	/**
	 * 获取：地址
	 */
	public String getDizhi() {
		return dizhi;
	}
				
	
	/**
	 * 设置：订单时间
	 */
	 
	public void setDingdanshijian(Date dingdanshijian) {
		this.dingdanshijian = dingdanshijian;
	}
	
	/**
	 * 获取：订单时间
	 */
	public Date getDingdanshijian() {
		return dingdanshijian;
	}
				
	
	/**
	 * 设置：订单备注
	 */
	 
	public void setDingdanbeizhu(String dingdanbeizhu) {
		this.dingdanbeizhu = dingdanbeizhu;
	}
	
	/**
	 * 获取：订单备注
	 */
	public String getDingdanbeizhu() {
		return dingdanbeizhu;
	}
				
	
	/**
	 * 设置：发布者
	 */
	 
	public void setXuehao(String xuehao) {
		this.xuehao = xuehao;
	}
	
	/**
	 * 获取：发布者
	 */
	public String getXuehao() {
		return xuehao;
	}
				
	
	/**
	 * 设置：代取者
	 */
	 
	public void setDaiquzhanghao(String daiquzhanghao) {
		this.daiquzhanghao = daiquzhanghao;
	}
	
	/**
	 * 获取：代取者
	 */
	public String getDaiquzhanghao() {
		return daiquzhanghao;
	}
				
	
	/**
	 * 设置：是否审核
	 */
	 
	public void setSfsh(String sfsh) {
		this.sfsh = sfsh;
	}
	
	/**
	 * 获取：是否审核
	 */
	public String getSfsh() {
		return sfsh;
	}
				
	
	/**
	 * 设置：审核回复
	 */
	 
	public void setShhf(String shhf) {
		this.shhf = shhf;
	}
	
	/**
	 * 获取：审核回复
	 */
	public String getShhf() {
		return shhf;
	}
				
	
	/**
	 * 设置：是否支付
	 */
	 
	public void setIspay(String ispay) {
		this.ispay = ispay;
	}
	
	/**
	 * 获取：是否支付
	 */
	public String getIspay() {
		return ispay;
	}
			
}
