package com.entity.vo;

import com.entity.DaigouxinxiEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
 

/**
 * 代购信息
 * 手机端接口返回实体辅助类 
 * （主要作用去除一些不必要的字段）
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public class DaigouxinxiVO  implements Serializable {
	private static final long serialVersionUID = 1L;

	 			
	/**
	 * 商品名称
	 */
	
	private String shangpinmingcheng;
		
	/**
	 * 照片
	 */
	
	private String zhaopian;
		
	/**
	 * 商品价格
	 */
	
	private Float shangpinjiage;
		
	/**
	 * 商品重量
	 */
	
	private Float shangpinzhongliang;
		
	/**
	 * 超重费用
	 */
	
	private Float chaozhongfeiyong;
		
	/**
	 * 收货地址
	 */
	
	private String shouhuodizhi;
		
	/**
	 * 购物地址
	 */
	
	private String gouwudizhi;
		
	/**
	 * 购物说明
	 */
	
	private String gouwushuoming;
		
	/**
	 * 最晚完成时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date zuiwanwanchengshijian;
		
	/**
	 * 发布时间
	 */
		
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 
	private Date fabushijian;
		
	/**
	 * 学号
	 */
	
	private String xuehao;
		
	/**
	 * 姓名
	 */
	
	private String xingming;
		
	/**
	 * 联系电话
	 */
	
	private String lianxidianhua;
		
	/**
	 * 代购备注
	 */
	
	private String daigoubeizhu;
		
	/**
	 * 代购状态
	 */
	
	private String daigouzhuangtai;
		
	/**
	 * 是否审核
	 */
	
	private String sfsh;
		
	/**
	 * 审核回复
	 */
	
	private String shhf;
				
	
	/**
	 * 设置：商品名称
	 */
	 
	public void setShangpinmingcheng(String shangpinmingcheng) {
		this.shangpinmingcheng = shangpinmingcheng;
	}
	
	/**
	 * 获取：商品名称
	 */
	public String getShangpinmingcheng() {
		return shangpinmingcheng;
	}
				
	
	/**
	 * 设置：照片
	 */
	 
	public void setZhaopian(String zhaopian) {
		this.zhaopian = zhaopian;
	}
	
	/**
	 * 获取：照片
	 */
	public String getZhaopian() {
		return zhaopian;
	}
				
	
	/**
	 * 设置：商品价格
	 */
	 
	public void setShangpinjiage(Float shangpinjiage) {
		this.shangpinjiage = shangpinjiage;
	}
	
	/**
	 * 获取：商品价格
	 */
	public Float getShangpinjiage() {
		return shangpinjiage;
	}
				
	
	/**
	 * 设置：商品重量
	 */
	 
	public void setShangpinzhongliang(Float shangpinzhongliang) {
		this.shangpinzhongliang = shangpinzhongliang;
	}
	
	/**
	 * 获取：商品重量
	 */
	public Float getShangpinzhongliang() {
		return shangpinzhongliang;
	}
				
	
	/**
	 * 设置：超重费用
	 */
	 
	public void setChaozhongfeiyong(Float chaozhongfeiyong) {
		this.chaozhongfeiyong = chaozhongfeiyong;
	}
	
	/**
	 * 获取：超重费用
	 */
	public Float getChaozhongfeiyong() {
		return chaozhongfeiyong;
	}
				
	
	/**
	 * 设置：收货地址
	 */
	 
	public void setShouhuodizhi(String shouhuodizhi) {
		this.shouhuodizhi = shouhuodizhi;
	}
	
	/**
	 * 获取：收货地址
	 */
	public String getShouhuodizhi() {
		return shouhuodizhi;
	}
				
	
	/**
	 * 设置：购物地址
	 */
	 
	public void setGouwudizhi(String gouwudizhi) {
		this.gouwudizhi = gouwudizhi;
	}
	
	/**
	 * 获取：购物地址
	 */
	public String getGouwudizhi() {
		return gouwudizhi;
	}
				
	
	/**
	 * 设置：购物说明
	 */
	 
	public void setGouwushuoming(String gouwushuoming) {
		this.gouwushuoming = gouwushuoming;
	}
	
	/**
	 * 获取：购物说明
	 */
	public String getGouwushuoming() {
		return gouwushuoming;
	}
				
	
	/**
	 * 设置：最晚完成时间
	 */
	 
	public void setZuiwanwanchengshijian(Date zuiwanwanchengshijian) {
		this.zuiwanwanchengshijian = zuiwanwanchengshijian;
	}
	
	/**
	 * 获取：最晚完成时间
	 */
	public Date getZuiwanwanchengshijian() {
		return zuiwanwanchengshijian;
	}
				
	
	/**
	 * 设置：发布时间
	 */
	 
	public void setFabushijian(Date fabushijian) {
		this.fabushijian = fabushijian;
	}
	
	/**
	 * 获取：发布时间
	 */
	public Date getFabushijian() {
		return fabushijian;
	}
				
	
	/**
	 * 设置：学号
	 */
	 
	public void setXuehao(String xuehao) {
		this.xuehao = xuehao;
	}
	
	/**
	 * 获取：学号
	 */
	public String getXuehao() {
		return xuehao;
	}
				
	
	/**
	 * 设置：姓名
	 */
	 
	public void setXingming(String xingming) {
		this.xingming = xingming;
	}
	
	/**
	 * 获取：姓名
	 */
	public String getXingming() {
		return xingming;
	}
				
	
	/**
	 * 设置：联系电话
	 */
	 
	public void setLianxidianhua(String lianxidianhua) {
		this.lianxidianhua = lianxidianhua;
	}
	
	/**
	 * 获取：联系电话
	 */
	public String getLianxidianhua() {
		return lianxidianhua;
	}
				
	
	/**
	 * 设置：代购备注
	 */
	 
	public void setDaigoubeizhu(String daigoubeizhu) {
		this.daigoubeizhu = daigoubeizhu;
	}
	
	/**
	 * 获取：代购备注
	 */
	public String getDaigoubeizhu() {
		return daigoubeizhu;
	}
				
	
	/**
	 * 设置：代购状态
	 */
	 
	public void setDaigouzhuangtai(String daigouzhuangtai) {
		this.daigouzhuangtai = daigouzhuangtai;
	}
	
	/**
	 * 获取：代购状态
	 */
	public String getDaigouzhuangtai() {
		return daigouzhuangtai;
	}
				
	
	/**
	 * 设置：是否审核
	 */
	 
	public void setSfsh(String sfsh) {
		this.sfsh = sfsh;
	}
	
	/**
	 * 获取：是否审核
	 */
	public String getSfsh() {
		return sfsh;
	}
				
	
	/**
	 * 设置：审核回复
	 */
	 
	public void setShhf(String shhf) {
		this.shhf = shhf;
	}
	
	/**
	 * 获取：审核回复
	 */
	public String getShhf() {
		return shhf;
	}
			
}
