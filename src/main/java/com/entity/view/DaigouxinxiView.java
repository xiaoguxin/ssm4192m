package com.entity.view;

import com.entity.DaigouxinxiEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 代购信息
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@TableName("daigouxinxi")
public class DaigouxinxiView  extends DaigouxinxiEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public DaigouxinxiView(){
	}
 
 	public DaigouxinxiView(DaigouxinxiEntity daigouxinxiEntity){
 	try {
			BeanUtils.copyProperties(this, daigouxinxiEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
