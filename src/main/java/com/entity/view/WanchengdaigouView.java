package com.entity.view;

import com.entity.WanchengdaigouEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 完成代购
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
@TableName("wanchengdaigou")
public class WanchengdaigouView  extends WanchengdaigouEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public WanchengdaigouView(){
	}
 
 	public WanchengdaigouView(WanchengdaigouEntity wanchengdaigouEntity){
 	try {
			BeanUtils.copyProperties(this, wanchengdaigouEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
