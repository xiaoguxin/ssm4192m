package com.dao;

import com.entity.DaiquxinxiEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DaiquxinxiVO;
import com.entity.view.DaiquxinxiView;


/**
 * 代取信息
 * 
 * @author 
 * @email 
 * @date 2023-05-11 17:05:49
 */
public interface DaiquxinxiDao extends BaseMapper<DaiquxinxiEntity> {
	
	List<DaiquxinxiVO> selectListVO(@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);
	
	DaiquxinxiVO selectVO(@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);
	
	List<DaiquxinxiView> selectListView(@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);

	List<DaiquxinxiView> selectListView(Pagination page,@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);
	
	DaiquxinxiView selectView(@Param("ew") Wrapper<DaiquxinxiEntity> wrapper);
	

}
