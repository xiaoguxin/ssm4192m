package com.dao;

import com.entity.DaiqudingdanEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DaiqudingdanVO;
import com.entity.view.DaiqudingdanView;


/**
 * 代取订单
 * 
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DaiqudingdanDao extends BaseMapper<DaiqudingdanEntity> {
	
	List<DaiqudingdanVO> selectListVO(@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);
	
	DaiqudingdanVO selectVO(@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);
	
	List<DaiqudingdanView> selectListView(@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);

	List<DaiqudingdanView> selectListView(Pagination page,@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);
	
	DaiqudingdanView selectView(@Param("ew") Wrapper<DaiqudingdanEntity> wrapper);
	

}
