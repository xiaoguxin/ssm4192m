package com.dao;

import com.entity.DiscussdaiquxinxiEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DiscussdaiquxinxiVO;
import com.entity.view.DiscussdaiquxinxiView;


/**
 * 代取信息评论表
 * 
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DiscussdaiquxinxiDao extends BaseMapper<DiscussdaiquxinxiEntity> {
	
	List<DiscussdaiquxinxiVO> selectListVO(@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);
	
	DiscussdaiquxinxiVO selectVO(@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);
	
	List<DiscussdaiquxinxiView> selectListView(@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);

	List<DiscussdaiquxinxiView> selectListView(Pagination page,@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);
	
	DiscussdaiquxinxiView selectView(@Param("ew") Wrapper<DiscussdaiquxinxiEntity> wrapper);
	

}
