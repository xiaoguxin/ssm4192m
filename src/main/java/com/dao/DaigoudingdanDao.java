package com.dao;

import com.entity.DaigoudingdanEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DaigoudingdanVO;
import com.entity.view.DaigoudingdanView;


/**
 * 代购订单
 * 
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DaigoudingdanDao extends BaseMapper<DaigoudingdanEntity> {
	
	List<DaigoudingdanVO> selectListVO(@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);
	
	DaigoudingdanVO selectVO(@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);
	
	List<DaigoudingdanView> selectListView(@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);

	List<DaigoudingdanView> selectListView(Pagination page,@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);
	
	DaigoudingdanView selectView(@Param("ew") Wrapper<DaigoudingdanEntity> wrapper);
	

}
