package com.dao;

import com.entity.DaigouxinxiEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.DaigouxinxiVO;
import com.entity.view.DaigouxinxiView;


/**
 * 代购信息
 * 
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface DaigouxinxiDao extends BaseMapper<DaigouxinxiEntity> {
	
	List<DaigouxinxiVO> selectListVO(@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);
	
	DaigouxinxiVO selectVO(@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);
	
	List<DaigouxinxiView> selectListView(@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);

	List<DaigouxinxiView> selectListView(Pagination page,@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);
	
	DaigouxinxiView selectView(@Param("ew") Wrapper<DaigouxinxiEntity> wrapper);
	

}
