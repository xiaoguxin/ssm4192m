package com.dao;

import com.entity.WanchengdaigouEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.entity.vo.WanchengdaigouVO;
import com.entity.view.WanchengdaigouView;


/**
 * 完成代购
 * 
 * @author 
 * @email 
 * @date 2023-05-11 17:05:50
 */
public interface WanchengdaigouDao extends BaseMapper<WanchengdaigouEntity> {
	
	List<WanchengdaigouVO> selectListVO(@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);
	
	WanchengdaigouVO selectVO(@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);
	
	List<WanchengdaigouView> selectListView(@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);

	List<WanchengdaigouView> selectListView(Pagination page,@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);
	
	WanchengdaigouView selectView(@Param("ew") Wrapper<WanchengdaigouEntity> wrapper);
	

}
