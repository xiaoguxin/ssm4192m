export function isAuth(tableName, key) {
  let role = localStorage.getItem("UserTableName");
  let menus = [{"backMenu":[{"child":[{"appFrontIcon":"cuIcon-explore","buttons":["新增","查看","修改","删除"],"menu":"学生","menuJump":"列表","tableName":"xuesheng"}],"menu":"学生管理"},{"child":[{"appFrontIcon":"cuIcon-goodsnew","buttons":["查看","修改","删除","审核","查看评论"],"menu":"代取信息","menuJump":"列表","tableName":"daiquxinxi"}],"menu":"代取信息管理"},{"child":[{"appFrontIcon":"cuIcon-full","buttons":["查看","修改","删除","审核"],"menu":"代购信息","menuJump":"列表","tableName":"daigouxinxi"}],"menu":"代购信息管理"},{"child":[{"appFrontIcon":"cuIcon-phone","buttons":["查看","修改","删除"],"menu":"代取订单","menuJump":"列表","tableName":"daiqudingdan"}],"menu":"代取订单管理"},{"child":[{"appFrontIcon":"cuIcon-pic","buttons":["查看","修改","删除"],"menu":"代购订单","menuJump":"列表","tableName":"daigoudingdan"}],"menu":"代购订单管理"},{"child":[{"appFrontIcon":"cuIcon-discover","buttons":["查看","修改","删除"],"menu":"完成代购","menuJump":"列表","tableName":"wanchengdaigou"}],"menu":"完成代购管理"},{"child":[{"appFrontIcon":"cuIcon-goods","buttons":["查看","修改","删除"],"menu":"轮播图管理","tableName":"config"},{"appFrontIcon":"cuIcon-news","buttons":["新增","查看","修改","删除"],"menu":"公告资讯","tableName":"news"},{"appFrontIcon":"cuIcon-similar","buttons":["查看","修改"],"menu":"关于我们","tableName":"aboutus"},{"appFrontIcon":"cuIcon-circle","buttons":["查看","修改"],"menu":"系统简介","tableName":"systemintro"}],"menu":"系统管理"}],"frontMenu":[{"child":[{"appFrontIcon":"cuIcon-attentionfavor","buttons":["查看","查看评论","代取"],"menu":"代取信息列表","menuJump":"列表","tableName":"daiquxinxi"}],"menu":"代取信息模块"},{"child":[{"appFrontIcon":"cuIcon-skin","buttons":["查看","代购"],"menu":"代购信息列表","menuJump":"列表","tableName":"daigouxinxi"}],"menu":"代购信息模块"}],"hasBackLogin":"是","hasBackRegister":"否","hasFrontLogin":"否","hasFrontRegister":"否","roleName":"管理员","tableName":"users"},{"backMenu":[{"child":[{"appFrontIcon":"cuIcon-goodsnew","buttons":["新增","查看","修改","删除"],"menu":"代取信息","menuJump":"列表","tableName":"daiquxinxi"}],"menu":"代取信息管理"},{"child":[{"appFrontIcon":"cuIcon-full","buttons":["新增","查看","修改","删除"],"menu":"代购信息","menuJump":"列表","tableName":"daigouxinxi"}],"menu":"代购信息管理"},{"child":[{"appFrontIcon":"cuIcon-phone","buttons":["查看","审核","支付","删除"],"menu":"代取订单","menuJump":"列表","tableName":"daiqudingdan"}],"menu":"代取订单管理"},{"child":[{"appFrontIcon":"cuIcon-pic","buttons":["查看","删除","审核","支付","完成代购"],"menu":"代购订单","menuJump":"列表","tableName":"daigoudingdan"}],"menu":"代购订单管理"},{"child":[{"appFrontIcon":"cuIcon-discover","buttons":["查看","删除","审核","支付"],"menu":"完成代购","menuJump":"列表","tableName":"wanchengdaigou"}],"menu":"完成代购管理"}],"frontMenu":[{"child":[{"appFrontIcon":"cuIcon-attentionfavor","buttons":["查看","查看评论","代取"],"menu":"代取信息列表","menuJump":"列表","tableName":"daiquxinxi"}],"menu":"代取信息模块"},{"child":[{"appFrontIcon":"cuIcon-skin","buttons":["查看","代购"],"menu":"代购信息列表","menuJump":"列表","tableName":"daigouxinxi"}],"menu":"代购信息模块"}],"hasBackLogin":"是","hasBackRegister":"否","hasFrontLogin":"是","hasFrontRegister":"是","roleName":"学生","tableName":"xuesheng"}];
  for(let i=0;i<menus.length;i++){
    if(menus[i].tableName==role){
      for(let j=0;j<menus[i].frontMenu.length;j++){
          for(let k=0;k<menus[i].frontMenu[j].child.length;k++){
            if(tableName==menus[i].frontMenu[j].child[k].tableName){
              let buttons = menus[i].frontMenu[j].child[k].buttons.join(',');
              return buttons.indexOf(key) !== -1 || false
            }
          }
      }
    }
  }
  return false;
}

/**
 *  * 获取当前时间（yyyy-MM-dd hh:mm:ss）
 *   */
export function getCurDateTime() {
    let currentTime = new Date(),
    year = currentTime.getFullYear(),
    month = currentTime.getMonth() + 1 < 10 ? '0' + (currentTime.getMonth() + 1) : currentTime.getMonth() + 1,
    day = currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate(),
    hour = currentTime.getHours(),
    minute = currentTime.getMinutes(),
    second = currentTime.getSeconds();
    return year + "-" + month + "-" + day + " " +hour +":" +minute+":"+second;
}

/**
 *  * 获取当前日期（yyyy-MM-dd）
 *   */
export function getCurDate() {
    let currentTime = new Date(),
    year = currentTime.getFullYear(),
    month = currentTime.getMonth() + 1 < 10 ? '0' + (currentTime.getMonth() + 1) : currentTime.getMonth() + 1,
    day = currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate();
    return year + "-" + month + "-" + day;
}
