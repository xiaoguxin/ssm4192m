export default {
    baseUrl: 'http://localhost:8080/ssm4192m/',
    indexNav: [
        {
            name: '首页',
            url: '/index/home'
        },
        {
            name: '代取信息',
            url: '/index/daiquxinxi'
        },
        {
            name: '代购信息',
            url: '/index/daigouxinxi'
        },
        {
            name: '公告资讯',
            url: '/index/news'
        },
    ]
}
