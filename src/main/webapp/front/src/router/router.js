import VueRouter from 'vue-router'

//引入组件
import Index from '../pages'
import Home from '../pages/home/home'
import Login from '../pages/login/login'
import Register from '../pages/register/register'
import Center from '../pages/center/center'
import News from '../pages/news/news-list'
import NewsDetail from '../pages/news/news-detail'
import xueshengList from '../pages/xuesheng/list'
import xueshengDetail from '../pages/xuesheng/detail'
import xueshengAdd from '../pages/xuesheng/add'
import daiquxinxiList from '../pages/daiquxinxi/list'
import daiquxinxiDetail from '../pages/daiquxinxi/detail'
import daiquxinxiAdd from '../pages/daiquxinxi/add'
import daigouxinxiList from '../pages/daigouxinxi/list'
import daigouxinxiDetail from '../pages/daigouxinxi/detail'
import daigouxinxiAdd from '../pages/daigouxinxi/add'
import daiqudingdanList from '../pages/daiqudingdan/list'
import daiqudingdanDetail from '../pages/daiqudingdan/detail'
import daiqudingdanAdd from '../pages/daiqudingdan/add'
import daigoudingdanList from '../pages/daigoudingdan/list'
import daigoudingdanDetail from '../pages/daigoudingdan/detail'
import daigoudingdanAdd from '../pages/daigoudingdan/add'
import wanchengdaigouList from '../pages/wanchengdaigou/list'
import wanchengdaigouDetail from '../pages/wanchengdaigou/detail'
import wanchengdaigouAdd from '../pages/wanchengdaigou/add'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
}

//配置路由
export default new VueRouter({
	routes:[
		{
      path: '/',
      redirect: '/index/home'
    },
		{
			path: '/index',
			component: Index,
			children:[
				{
					path: 'home',
					component: Home
				},
				{
					path: 'center',
					component: Center,
				},
				{
					path: 'news',
					component: News
				},
				{
					path: 'newsDetail',
					component: NewsDetail
				},
				{
					path: 'xuesheng',
					component: xueshengList
				},
				{
					path: 'xueshengDetail',
					component: xueshengDetail
				},
				{
					path: 'xueshengAdd',
					component: xueshengAdd
				},
				{
					path: 'daiquxinxi',
					component: daiquxinxiList
				},
				{
					path: 'daiquxinxiDetail',
					component: daiquxinxiDetail
				},
				{
					path: 'daiquxinxiAdd',
					component: daiquxinxiAdd
				},
				{
					path: 'daigouxinxi',
					component: daigouxinxiList
				},
				{
					path: 'daigouxinxiDetail',
					component: daigouxinxiDetail
				},
				{
					path: 'daigouxinxiAdd',
					component: daigouxinxiAdd
				},
				{
					path: 'daiqudingdan',
					component: daiqudingdanList
				},
				{
					path: 'daiqudingdanDetail',
					component: daiqudingdanDetail
				},
				{
					path: 'daiqudingdanAdd',
					component: daiqudingdanAdd
				},
				{
					path: 'daigoudingdan',
					component: daigoudingdanList
				},
				{
					path: 'daigoudingdanDetail',
					component: daigoudingdanDetail
				},
				{
					path: 'daigoudingdanAdd',
					component: daigoudingdanAdd
				},
				{
					path: 'wanchengdaigou',
					component: wanchengdaigouList
				},
				{
					path: 'wanchengdaigouDetail',
					component: wanchengdaigouDetail
				},
				{
					path: 'wanchengdaigouAdd',
					component: wanchengdaigouAdd
				},
			]
		},
		{
			path: '/login',
			component: Login
		},
		{
			path: '/register',
			component: Register
		},
	]
})
