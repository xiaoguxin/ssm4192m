const base = {
    get() {
        return {
            url : "http://localhost:8080/ssm4192m/",
            name: "ssm4192m",
            // 退出到首页链接
            indexUrl: 'http://localhost:8080/ssm4192m/front/dist/index.html'
        };
    },
    getProjectName(){
        return {
            projectName: "校园跑腿平台"
        } 
    }
}
export default base
